﻿using codenameORCA2.ActionLog;
using System.Collections.Generic;
using codenameORCA2.State;

namespace codenameORCA2.Job
{
    public interface IOrcaJob : IOrcaState
    {
        List<IOrcaAction> OrcaActions { get; set; }
    }
}