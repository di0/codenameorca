﻿using codenameORCA2.ActionLog;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using codenameORCA2.State;

namespace codenameORCA2.Job
{
    internal class OrcaJob : IOrcaJob
    {
        public List<IOrcaAction> OrcaActions { get; set; }

        #region IOrcaState Implementations

        public CancellationToken CancelToken { get; set; }
        public OrcaStateType State { get; set; }
        public void Fail(string failMessage)
        {
            throw new System.NotImplementedException();
        }

        public void Start()
        {
            throw new System.NotImplementedException();
        }

        public async Task StartAsync()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }

        public void Succeed()
        {
            throw new System.NotImplementedException();
        }

        public void SucceedWithWarning(string warnMessage)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}