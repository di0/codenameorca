﻿namespace codenameORCA2.State
{
    // TODO: implement transition type when i do a proper state machine with MoveNext and such
    public enum OrcaStateTransitionType
    {
        Start,
        Succeed,
        SucceedWithWarning,
        FailWithError
    }
}