﻿namespace codenameORCA2.State
{
    // This state machine is designed for use with both Jobs and Actions (Fsm = Finite State Machine)

    public enum OrcaStateType
    {
        Stopped,
        Started,
        Succeeded,
        SucceededWithWarning,
        Failed
    }
}