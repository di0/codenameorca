﻿using System.Threading;
using System.Threading.Tasks;

namespace codenameORCA2.State
{
    public interface IOrcaState
    {
        CancellationToken CancelToken { get; set; }

        OrcaStateType State { get; set; }

        void Fail(string failMessage);

        void Start();

        Task StartAsync();

        void Stop();

        void Succeed();

        void SucceedWithWarning(string warnMessage);
    }
}