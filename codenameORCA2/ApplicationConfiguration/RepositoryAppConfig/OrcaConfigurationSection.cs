﻿using codenameORCA2.ApplicationLog;
using System;
using System.Configuration;

namespace codenameORCA2.ApplicationConfiguration.RepositoryAppConfig
{
    public class OrcaConfigurationSection : ConfigurationSection
    {
        public const string LABEL       = "orca";

        //public const string ORCA_TARGETHOSTS_SECTION             = "targetHosts";
        public const string ORCA_APPLICATIONLOG_SECTION            = "applicationLog";

        public const string ORCA_JOBS_SECTION                      = "jobs";

        [ConfigurationProperty(ORCA_APPLICATIONLOG_SECTION)]
        public OrcaApplicationLogElement OrcaApplicationLogSetting
        {
            get
            {
                return (OrcaApplicationLogElement)base[ORCA_APPLICATIONLOG_SECTION];
            }
        }

        [ConfigurationProperty(ORCA_JOBS_SECTION)]
        public OrcaJobsElementCollection OrcaJobs
        {
            get
            {
                return (OrcaJobsElementCollection)base[ORCA_JOBS_SECTION];
            }
        }

        /*
        [ConfigurationProperty(ORCA_TARGETHOSTS_SECTION)]
        public OrcaTargetHostsCollection OrcaTargetHosts
        {
            get
            {
                return (OrcaTargetHostsCollection)base[ORCA_TARGETHOSTS_SECTION];
            }
        }
        */

        public static OrcaConfigurationSection Get()
        {
            try
            {
                return (OrcaConfigurationSection)
                    ConfigurationManager.GetSection(LABEL);
            }
            catch (Exception e)
            {
                OrcaApplicationLoggerGlobal.Log(OrcaApplicationLogUtilities.GetLoggingHeader(),
                    e.Message, OrcaApplicationLogLevel.Fatal);

                throw new SystemException("Cannot parse invalid app.config. Cannot proceed.");
            }
        }
    }
}