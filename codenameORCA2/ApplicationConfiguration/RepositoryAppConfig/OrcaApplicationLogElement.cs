﻿using codenameORCA2.ApplicationLog;
using System;
using System.Configuration;

namespace codenameORCA2.ApplicationConfiguration.RepositoryAppConfig
{
    public class OrcaApplicationLogElement : ConfigurationElement
    {
        public const string MAX_LEVEL                           = "maxLevel";
        public const string MIN_LEVEL                           = "minLevel";
        public const string UNIQUE_POLICY_NAME_PROPERTY         = "logLevelRange";

        [ConfigurationProperty(MAX_LEVEL, IsRequired = false)]
        public OrcaApplicationLogLevel MaxLevel
        {
            get
            {
                OrcaApplicationLogLevel DEFAULT =
                    OrcaApplicationLoggerGlobal.DEFAULT_MAXLEVEL;

                try
                {
                    if (string.IsNullOrWhiteSpace(this[MAX_LEVEL]?.ToString()))
                    {
                        return DEFAULT;
                    }
                }
                catch (Exception e)
                {
                    OrcaApplicationLoggerGlobal.Log
                        (OrcaApplicationLogUtilities.GetLoggingHeader(),
                            e.Message, OrcaApplicationLogLevel.Warning);
                }

                OrcaApplicationLogLevel parsed;

                if (Enum.TryParse(this[MAX_LEVEL].ToString(), out parsed))
                {
                    OrcaApplicationLoggerGlobal.Log
                    (OrcaApplicationLogUtilities.GetLoggingHeader(),
                        $"Successfully parsed {nameof(MaxLevel)}: '{parsed}'", OrcaApplicationLogLevel.Debug);

                    return parsed;
                }
                else
                {
                    return DEFAULT;
                }
            }
            set
            {
                this[MAX_LEVEL] = value;
            }
        }

        [ConfigurationProperty(MIN_LEVEL, IsRequired = false)]
        public OrcaApplicationLogLevel MinLevel
        {
            get
            {
                OrcaApplicationLogLevel DEFAULT =
                    OrcaApplicationLoggerGlobal.DEFAULT_MINLEVEL;

                try
                {
                    if (string.IsNullOrWhiteSpace(this[MIN_LEVEL]?.ToString()))
                    {
                        return DEFAULT;
                    }
                }
                catch (Exception e)
                {
                    OrcaApplicationLoggerGlobal.Log
                        (OrcaApplicationLogUtilities.GetLoggingHeader(),
                            e.Message, OrcaApplicationLogLevel.Warning);
                }

                OrcaApplicationLogLevel parsed;

                if (Enum.TryParse(this[MIN_LEVEL].ToString(), out parsed))
                {
                    OrcaApplicationLoggerGlobal.Log
                    (OrcaApplicationLogUtilities.GetLoggingHeader(),
                        $"Successfully parsed {nameof(MinLevel)}: '{parsed}'", OrcaApplicationLogLevel.Debug);

                    return parsed;
                }
                else
                {
                    return DEFAULT;
                }
            }
            set
            {
                this[MIN_LEVEL] = value;
            }
        }
    }
}