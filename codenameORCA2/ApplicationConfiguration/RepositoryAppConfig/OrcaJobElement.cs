﻿using System;
using System.Configuration;

namespace codenameORCA2.ApplicationConfiguration.RepositoryAppConfig
{
    public class OrcaJobElement : ConfigurationElement
    {
        public const string MAXIMUM_TABLE_RECORD_AGE_PROPERTY   = "maximumTableRecordAge";
        public const string MAXIMUM_TABLE_ROW_COUNT_PROPERTY    = "maximumTableRowCount";
        public const string MAXIMUM_TABLE_SIZE_ON_DISK_PROPERTY = "maximumTableSizeOnDisk";
        public const string UNIQUE_POLICY_NAME_PROPERTY         = "uniquePolicyName";

        [ConfigurationProperty(MAXIMUM_TABLE_RECORD_AGE_PROPERTY, IsRequired = false)]
        public string MaximumTableRecordAge
        {
            get
            {
                if (this[MAXIMUM_TABLE_RECORD_AGE_PROPERTY] == null) // can't ToString() if it's null
                    return null;
                else
                    return this[MAXIMUM_TABLE_RECORD_AGE_PROPERTY].ToString();
            }
            set
            {
                this[MAXIMUM_TABLE_RECORD_AGE_PROPERTY] = value;
            }
        }

        [ConfigurationProperty(MAXIMUM_TABLE_ROW_COUNT_PROPERTY, IsRequired = false)]
        public int? MaximumTableRowCount
        {
            get
            {
                if (this[MAXIMUM_TABLE_ROW_COUNT_PROPERTY] == null) // can't ToString() if it's null
                    return null;

                int maximumRowCount;

                if (Int32.TryParse(this[MAXIMUM_TABLE_ROW_COUNT_PROPERTY].ToString(), out maximumRowCount))
                    return maximumRowCount;
                else
                    return null;
            }
            set
            {
                this[MAXIMUM_TABLE_ROW_COUNT_PROPERTY] = value;
            }
        }

        [ConfigurationProperty(MAXIMUM_TABLE_SIZE_ON_DISK_PROPERTY, IsRequired = false)]
        public int? MaximumTableSizeOnDisk
        {
            get
            {
                if (this[MAXIMUM_TABLE_SIZE_ON_DISK_PROPERTY] == null) // can't ToString() if it's null
                    return null;

                int maximumSizeOnDisk;

                if (Int32.TryParse(this[MAXIMUM_TABLE_SIZE_ON_DISK_PROPERTY].ToString(), out maximumSizeOnDisk))
                    return maximumSizeOnDisk;
                else
                    return null;
            }
            set
            {
                this[MAXIMUM_TABLE_SIZE_ON_DISK_PROPERTY] = value;
            }
        }

        [ConfigurationProperty(UNIQUE_POLICY_NAME_PROPERTY, IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/:;'\"|\\")]
        public string UniquePolicyName
        {
            get
            {
                return this[UNIQUE_POLICY_NAME_PROPERTY] as string;
            }
            set
            {
                this[UNIQUE_POLICY_NAME_PROPERTY] = value;
            }
        }
    }
}