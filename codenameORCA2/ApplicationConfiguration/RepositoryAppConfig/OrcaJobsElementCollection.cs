﻿using System.Configuration;

namespace codenameORCA2.ApplicationConfiguration.RepositoryAppConfig
{
    public class OrcaJobsElementCollection : ConfigurationElementCollection
    {
        public OrcaJobElement this[int index]
        {
            get
            {
                OrcaJobElement orcaJobElement;

                try
                {
                    orcaJobElement = (OrcaJobElement)BaseGet(index);
                }
                catch (ConfigurationErrorsException)
                {
                    return null;
                }

                return orcaJobElement;
            }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);

                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new OrcaJobElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            // this value doesn't really matter in our implementations, since we bypass this
            //    method when retrieving a value, but the abstract class we're deriving from
            //    requires this override method -- dujohnson
            return ((OrcaJobElement)element).UniquePolicyName;
        }
    }
}