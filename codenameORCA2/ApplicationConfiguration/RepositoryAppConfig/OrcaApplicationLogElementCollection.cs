﻿using System;
using System.Configuration;

namespace codenameORCA2.ApplicationConfiguration.RepositoryAppConfig
{
    public class OrcaApplicationLogElementCollection : ConfigurationElementCollection
    {
        public OrcaApplicationLogElement this[int index]
        {
            get
            {
                OrcaApplicationLogElement orcaLoggingElement;

                try
                {
                    orcaLoggingElement = (OrcaApplicationLogElement)BaseGet(index);
                }
                catch (ConfigurationErrorsException)
                {
                    return null;
                }

                return orcaLoggingElement;
            }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);

                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new OrcaApplicationLogElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            // this value doesn't really matter in our implementations, since we bypass this
            //    method when retrieving a value, but the abstract class we're deriving from
            //    requires this override method -- dujohnson
            //return ((OrcaApplicationLogElement)element).MaxLevel;
            throw new NotImplementedException();
        }
    }
}