﻿using codenameORCA2.ApplicationLog;
using System;
using System.Configuration;

namespace codenameORCA2.ApplicationConfiguration.RepositoryAppConfig
{
    /*

    <jobs>
      <job name="my first ORCA job">
        <length minutes="10"/>
        <startTime ticks="" />
        <!-- <replay file="c:\\somepath\\somefolder\\somefile.job" /> -->
      </job>
    </jobs>

     */

    public class OrcaApplicationConfigurationRepositoryAppConfig : IOrcaApplicationConfigurationRepository
    {
        public IOrcaApplicationConfiguration DeserializeRepository(
            IOrcaApplicationConfiguration orcaApplicationConfiguration)
        {
            // first, let's refresh the app.config jic something has changed since the app started
            refreshOrcaConfigurationSection();

            //
            // Configure ORCA Logging for this OrcaInstanceConfiguration
            //

            // get the ORCA application log level for GlobalLogger from config file
            orcaApplicationConfiguration.ApplicationLoggerLevelRange = getApplicationLoggerLevelRange();

            //
            // Configure OrcaInstanceConfigurations
            //

            //
            // Configure ORCA Jobs
            //

            // ---- TODO: load from app.config

            //
            // Configure ORCA Targets
            //

            // ---- TODO: load from app.config

            //
            // Return the Initialized ORCA Instance Configuration
            //

            return orcaApplicationConfiguration;
        }

        private static void refreshOrcaConfigurationSection()
        {
            ConfigurationManager.RefreshSection(OrcaConfigurationSection.LABEL);
        }

        private Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> getApplicationLoggerLevelRange()
        {
            var orcaConfigurationSection = OrcaConfigurationSection.Get();

            OrcaApplicationLogLevel minLevel =
                orcaConfigurationSection.OrcaApplicationLogSetting.MinLevel;

            OrcaApplicationLogLevel maxLevel =
                orcaConfigurationSection.OrcaApplicationLogSetting.MaxLevel;

            return new Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel>
                    (minLevel, maxLevel);
        }
    }
}