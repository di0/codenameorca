﻿namespace codenameORCA2.ApplicationConfiguration
{
    public enum OrcaApplicationModeType
    {
        Solo,
        Leader,
        Worker,
        ReplayLeader,
        ReplayWorker
    }
}