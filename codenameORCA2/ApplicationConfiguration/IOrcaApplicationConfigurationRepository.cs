﻿namespace codenameORCA2.ApplicationConfiguration
{
    internal interface IOrcaApplicationConfigurationRepository
    {
        IOrcaApplicationConfiguration DeserializeRepository(
            IOrcaApplicationConfiguration orcaApplicationConfiguration);
    }
}