﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.Instance;
using System;
using System.Collections.Generic;

namespace codenameORCA2.ApplicationConfiguration
{
    public class OrcaApplicationConfiguration : IOrcaApplicationConfiguration
    {
        public OrcaApplicationConfiguration()
        {
            OrcaInstanceConfigurations = new List<IOrcaInstanceConfiguration>();
        }

        public Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> ApplicationLoggerLevelRange { get; set; }
        public List<IOrcaInstanceConfiguration> OrcaInstanceConfigurations { get; set; }
    }
}