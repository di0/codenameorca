﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.Instance;
using System;
using System.Collections.Generic;

namespace codenameORCA2.ApplicationConfiguration
{
    public interface IOrcaApplicationConfiguration
    {
        Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> ApplicationLoggerLevelRange { get; set; }
        List<IOrcaInstanceConfiguration> OrcaInstanceConfigurations { get; set; }
    }
}