﻿using codenameORCA2.TargetHost.Resource.Catastrophe;
using System.Collections.Generic;

namespace codenameORCA2.TargetHost.Resource
{
    public interface IOrcaTargetHostResource
    {
        List<IOrcaTargetHostResourceCatastrophe> OrcaTargetHostResourceCatastrophies { get; set; }
        IOrcaTargetHostResourceConfiguration OrcaTargetHostResourceConfiguration { get; set; }
    }
}