﻿using System.Threading.Tasks;

namespace codenameORCA2.TargetHost.Resource.Catastrophe
{
    public interface IOrcaTargetHostResourceCatastrophe
    {
        IOrcaTargetHost TargetHost { get; set; }

        Task<bool> Payload();
    }
}