﻿using System;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace codenameORCA2.TargetHost.Resource.Catastrophe
{
    /// <summary>
    /// DESTRUCTIVE MODE ONLY --- MAY NOT EVEN BE A THING WE WANT TO DO LOL
    /// </summary>
    public class DatabaseSwissCheese : IOrcaTargetHostResourceCatastrophe
    {
        // write zeroes to a random part of a target database -- currently only MSSQL supported
        public IOrcaTargetHost TargetHost { get; set; }

        public Task<bool> Payload()
        {
            throw new NotImplementedException();
        }
    }

    public class DisruptiveLoadCatastrophyConfiguration : IOrcaTargetHostResourceCatastropheConfiguration
    {
    }

    public class DummyTargetHostResourceCatastrophe : IOrcaTargetHostResourceCatastrophe
    {
        public IOrcaTargetHost TargetHost
        {
            get => throw new System.NotImplementedException();
            set => throw new System.NotImplementedException();
        }

        public Task<bool> Payload()
        {
            throw new System.NotImplementedException();
        }
    }

    public class MsSqlAgentRestart : IOrcaTargetHostResourceCatastrophe
    {
        public IOrcaTargetHost TargetHost
        {
            get => throw new System.NotImplementedException();
            set => throw new System.NotImplementedException();
        }

        Task<bool> IOrcaTargetHostResourceCatastrophe.Payload()
        {
            throw new System.NotImplementedException();
        }
    }

    public class WindowsServiceRestart : IOrcaTargetHostResourceCatastrophe
    {
        private IOrcaTargetHost _orcaTargetHost;
        private ServiceController _serviceController;

        private WindowsServiceRestart(IOrcaTargetHost orcaTargetHost, ServiceController serviceController)
        {
            _orcaTargetHost = orcaTargetHost;
            _serviceController = serviceController;
        }

        public IOrcaTargetHost TargetHost
        {
            get => throw new System.NotImplementedException();
            set => throw new System.NotImplementedException();
        }

        public Task<bool> Payload()
        {
            throw new System.NotImplementedException();
        }
    }
}