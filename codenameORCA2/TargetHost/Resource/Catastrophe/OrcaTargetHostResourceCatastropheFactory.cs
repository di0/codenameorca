﻿using codenameORCA2.ApplicationLog;
using System;

namespace codenameORCA2.TargetHost.Resource.Catastrophe
{
    public static class OrcaTargetHostResourceCatastropheFactory
    {
        public static IOrcaTargetHostResourceCatastrophe Create(
            OrcaTargetHostResourceCatastropheType orcaTargetHostResourceCatastropheType)
        {
            switch (orcaTargetHostResourceCatastropheType)
            {
                case OrcaTargetHostResourceCatastropheType.Dummy:
                    return new DummyTargetHostResourceCatastrophe();

                case OrcaTargetHostResourceCatastropheType.DisruptiveLoad:
                    throw new NotImplementedException();

                case OrcaTargetHostResourceCatastropheType.WindowsServiceRestart:
                    throw new NotImplementedException();

                default:
                    throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());
            }
        }
    }
}