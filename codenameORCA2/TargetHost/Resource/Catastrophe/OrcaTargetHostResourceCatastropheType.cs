﻿namespace codenameORCA2.TargetHost.Resource.Catastrophe
{
    public enum OrcaTargetHostResourceCatastropheType
    {
        Dummy,
        WindowsServiceRestart,
        DisruptiveLoad
    }
}