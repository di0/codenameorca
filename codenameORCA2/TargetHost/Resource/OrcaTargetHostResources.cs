﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.TargetHost.Resource.Catastrophe;
using System;
using System.Collections.Generic;

namespace codenameORCA2.TargetHost.Resource
{
    public class OrcaTargetHostResourceFactory
    {
        public static IOrcaTargetHostResource Create(OrcaTargetHostResourceType orcaTargetHostResourceType,
            IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration)
        {
            switch (orcaTargetHostResourceType)
            {
                case OrcaTargetHostResourceType.Dummy:
                    return new OrcaTargetHostResources.Dummy(orcaTargetHostResourceConfiguration);

                case OrcaTargetHostResourceType.MicrosoftSql:
                    return new OrcaTargetHostResources.MicrosoftSql(orcaTargetHostResourceConfiguration);

                case OrcaTargetHostResourceType.OsiSoftAssetFramework:
                    return new OrcaTargetHostResources.OsiSoftAssetFramework(orcaTargetHostResourceConfiguration);

                case OrcaTargetHostResourceType.OsiSoftDataArchive:
                    return new OrcaTargetHostResources.OsiSoftDataArchive(orcaTargetHostResourceConfiguration);

                case OrcaTargetHostResourceType.WindowsService:
                    return new OrcaTargetHostResources.WindowsService(orcaTargetHostResourceConfiguration);

                default:
                    throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());
            }
        }
    }

    internal class OrcaTargetHostResources
    {
        public class Dummy : IOrcaTargetHostResource
        {
            public Dummy(IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration)
            {
                OrcaTargetHostResourceConfiguration = orcaTargetHostResourceConfiguration;
                OrcaTargetHostResourceCatastrophies = new List<IOrcaTargetHostResourceCatastrophe>();
            }

            public List<IOrcaTargetHostResourceCatastrophe> OrcaTargetHostResourceCatastrophies { get; set; }
            public IOrcaTargetHostResourceConfiguration OrcaTargetHostResourceConfiguration { get; set; }
        }

        public class MicrosoftSql : IOrcaTargetHostResource
        {
            public MicrosoftSql(IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration)
            {
                OrcaTargetHostResourceConfiguration = orcaTargetHostResourceConfiguration;
                OrcaTargetHostResourceCatastrophies = new List<IOrcaTargetHostResourceCatastrophe>();
            }

            public List<IOrcaTargetHostResourceCatastrophe> OrcaTargetHostResourceCatastrophies { get; set; }
            public IOrcaTargetHostResourceConfiguration OrcaTargetHostResourceConfiguration { get; set; }
        }

        public class OsiSoftAssetFramework : IOrcaTargetHostResource
        {
            public OsiSoftAssetFramework(IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration)
            {
                OrcaTargetHostResourceConfiguration = orcaTargetHostResourceConfiguration;
                OrcaTargetHostResourceCatastrophies = new List<IOrcaTargetHostResourceCatastrophe>();
            }

            public List<IOrcaTargetHostResourceCatastrophe> OrcaTargetHostResourceCatastrophies { get; set; }
            public IOrcaTargetHostResourceConfiguration OrcaTargetHostResourceConfiguration { get; set; }
        }

        public class OsiSoftDataArchive : IOrcaTargetHostResource
        {
            public OsiSoftDataArchive(IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration)
            {
                OrcaTargetHostResourceConfiguration = orcaTargetHostResourceConfiguration;
                OrcaTargetHostResourceCatastrophies = new List<IOrcaTargetHostResourceCatastrophe>();
            }

            public List<IOrcaTargetHostResourceCatastrophe> OrcaTargetHostResourceCatastrophies { get; set; }
            public IOrcaTargetHostResourceConfiguration OrcaTargetHostResourceConfiguration { get; set; }
        }

        public class WindowsService : IOrcaTargetHostResource
        {
            public WindowsService(IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration)
            {
                OrcaTargetHostResourceConfiguration = orcaTargetHostResourceConfiguration;
                OrcaTargetHostResourceCatastrophies = new List<IOrcaTargetHostResourceCatastrophe>();
            }

            public List<IOrcaTargetHostResourceCatastrophe> OrcaTargetHostResourceCatastrophies { get; set; }
            public IOrcaTargetHostResourceConfiguration OrcaTargetHostResourceConfiguration { get; set; }
        }
    }
}