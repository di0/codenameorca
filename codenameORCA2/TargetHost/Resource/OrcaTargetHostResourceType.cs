﻿namespace codenameORCA2.TargetHost.Resource
{
    public enum OrcaTargetHostResourceType
    {
        Dummy,
        MicrosoftSql,
        OsiSoftAssetFramework,
        OsiSoftDataArchive,
        WindowsService
    }
}