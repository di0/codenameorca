﻿using codenameORCA2.TargetHost.Resource;
using System.Collections.Generic;

namespace codenameORCA2.TargetHost
{
    public interface IOrcaTargetHostConfiguration
    {
        List<IOrcaTargetHostResource> OrcaTargetHostResources { get; set; }
    }
}