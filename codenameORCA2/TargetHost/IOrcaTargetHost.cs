﻿using codenameORCA2.TargetHost.Resource;
using codenameORCA2.TargetHost.Resource.Catastrophe;
using System.Threading.Tasks;

namespace codenameORCA2.TargetHost
{
    public interface IOrcaTargetHost
    {
        IOrcaTargetHostConfiguration OrcaTargetHostConfiguration { get; set; }

        // if the catastrophe isn't in the configuration list of supported catastrophes, then die
        Task<bool> TryCatastrophe(IOrcaTargetHostResource orcaTargetHostResource,
            IOrcaTargetHostResourceCatastrophe orcaTargetHostResourceCatastrophe);
    }
}