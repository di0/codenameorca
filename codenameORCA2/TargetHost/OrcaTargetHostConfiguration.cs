﻿using codenameORCA2.TargetHost.Resource;
using System.Collections.Generic;

namespace codenameORCA2.TargetHost
{
    public class OrcaTargetHostConfiguration : IOrcaTargetHostConfiguration
    {
        public OrcaTargetHostConfiguration()
        {
            OrcaTargetHostResources = new List<IOrcaTargetHostResource>();
        }

        public List<IOrcaTargetHostResource> OrcaTargetHostResources { get; set; }
    }
}