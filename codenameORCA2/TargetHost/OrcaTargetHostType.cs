﻿namespace codenameORCA2.TargetHost
{
    public enum OrcaTargetHostType
    {
        Dummy,
        WindowsMachine,
        LinuxMachine,
        MacOsMachine
    }
}