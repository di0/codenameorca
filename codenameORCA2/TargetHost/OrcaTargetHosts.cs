﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.TargetHost;
using codenameORCA2.TargetHost.Resource;
using codenameORCA2.TargetHost.Resource.Catastrophe;
using System;
using System.Threading.Tasks;

namespace codenameORCA2
{
    public static class OrcaTargetHosts
    {
        public class DummyTargetHost : IOrcaTargetHost
        {
            public DummyTargetHost(IOrcaTargetHostConfiguration orcaTargetHostConfiguration)
            {
                OrcaTargetHostConfiguration = orcaTargetHostConfiguration;
            }

            public IOrcaTargetHostConfiguration OrcaTargetHostConfiguration { get; set; }

            public Task<bool> TryCatastrophe(IOrcaTargetHostResource orcaTargetHostResource,
                IOrcaTargetHostResourceCatastrophe orcaTargetHostResourceCatastrophe)
            {
                OrcaApplicationLoggerGlobal.Log(OrcaApplicationLogUtilities.GetLoggingHeader(),
                    $"Tried ORCA Target Host Resource Catastrophe: '{orcaTargetHostResourceCatastrophe.GetType()}', " +
                    $"using ORCA Target Host Resource '{orcaTargetHostResource.GetType()}'," +
                    $"on ORCA Target Host: '{nameof(DummyTargetHost)}'.",
                    OrcaApplicationLogLevel.Info);

                return Task.FromResult(true);
            }
        }

        public class OrcaTargetHostFactory
        {
            public static IOrcaTargetHost Create(OrcaTargetHostType orcaTargetHostType, IOrcaTargetHostConfiguration orcaTargetHostConfiguration)
            {
                switch (orcaTargetHostType)
                {
                    case OrcaTargetHostType.Dummy:
                        return new OrcaTargetHosts.DummyTargetHost(orcaTargetHostConfiguration);

                    case OrcaTargetHostType.WindowsMachine:
                        return new OrcaTargetHosts.WindowsTargetHost(orcaTargetHostConfiguration);

                    case OrcaTargetHostType.LinuxMachine:
                        throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());

                    case OrcaTargetHostType.MacOsMachine:
                        throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());

                    default:
                        throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());
                }
            }
        }

        public class WindowsTargetHost : IOrcaTargetHost
        {
            public WindowsTargetHost(IOrcaTargetHostConfiguration orcaTargetHostConfiguration)
            {
                OrcaTargetHostConfiguration = orcaTargetHostConfiguration;
            }

            public IOrcaTargetHostConfiguration OrcaTargetHostConfiguration { get; set; }

            public Task<bool> TryCatastrophe(IOrcaTargetHostResource orcaTargetHostResource,
                IOrcaTargetHostResourceCatastrophe orcaTargetHostResourceCatastrophe)
            {
                throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());
            }
        }
    }
}