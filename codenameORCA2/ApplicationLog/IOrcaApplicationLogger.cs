﻿using System;

namespace codenameORCA2.ApplicationLog
{
    public interface IOrcaApplicationLogger
    {
        void Initialize(Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> globalLoggerLevelRange);

        void Log(string logHeader, string logMessage, OrcaApplicationLogLevel orcaApplicationLogLevel);
    }
}