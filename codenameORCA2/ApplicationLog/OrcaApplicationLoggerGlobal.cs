﻿using System;

namespace codenameORCA2.ApplicationLog
{
    public static class OrcaApplicationLoggerGlobal // follow interface IOrcaApplicationLogger
    {
        public const OrcaApplicationLogLevel DEFAULT_MAXLEVEL = OrcaApplicationLogLevel.Fatal;
        public const OrcaApplicationLogLevel DEFAULT_MINLEVEL = OrcaApplicationLogLevel.Debug;

        // this is the one and only usable instance of any IOrcaLogger
        //    implementation instance that exists at any given time for this assembly (2018.11.23)
        private static IOrcaApplicationLogger _orcaLoggerInstance;

        static OrcaApplicationLoggerGlobal()
        {
            Initialize(
                new Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel>
                    (DEFAULT_MINLEVEL, DEFAULT_MAXLEVEL));
        }

        /// <summary>
        /// Using the Initialize() method with the IOrcaLogger _orcaLoggerInstance allows
        ///    extensibility within GlobalLogger to allow the use different logging solutions
        ///    in the future, even while the app is open.
        /// </summary>
        /// <param name="globalLoggerLevelRange"></param>
        public static void Initialize(Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> globalLoggerLevelRange)
        {
            // Current Logging Solution: NLog
            _orcaLoggerInstance = new OrcaApplicationLoggerNLog(globalLoggerLevelRange);

            _orcaLoggerInstance.Log
            (OrcaApplicationLogUtilities.GetLoggingHeader(),
                $"{nameof(OrcaApplicationLoggerNLog)} has been initialized.",
                OrcaApplicationLogLevel.Info);
        }

        public static void Log(string logHeader, string logMessage, OrcaApplicationLogLevel orcaApplicationLogLevel)
        {
            // header must be added from the calling code to get accurate outerclass/innerclass/member name
            _orcaLoggerInstance.Log(logHeader, logMessage, orcaApplicationLogLevel);
        }
    }
}