﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace codenameORCA2.ApplicationLog
{
    internal class OrcaApplicationLogUtilities
    {
        /// <summary>
        /// We only need one of these helper methods in the entire codebase, but for some reason,
        /// there are about 3million replicas of this logic. Begin refactoring to use only this one.
        /// </summary>
        /// <returns>The class name and the method name.</returns>
        public static string GetLoggingHeader()
        {
            // TODO: break this into GetClassName() and GetMethodName() and GetLoggingHeader()

            var headerStringBuilder = new StringBuilder();

            // get the stack trace
            StackFrame frame = new StackFrame(1);

            // we're working backwards from the current call location,
            //    so, let's start with the method that called this one
            string methodName = frame.GetMethod().Name; //Gets the current method name

            // now, let's derive everything else, based on that first method
            MethodBase method = frame.GetMethod();

            string className = method.DeclaringType?.Name; //Gets the current class name

            string outerClassName = method.DeclaringType?.DeclaringType?.Name;

            string outerOuterClassName = method.DeclaringType?.DeclaringType?.DeclaringType?.Name;

            // compose the header

            if (!string.IsNullOrWhiteSpace(outerOuterClassName))
            {
                headerStringBuilder.Append($"{outerOuterClassName}::");
            }

            if (!string.IsNullOrWhiteSpace(outerClassName))
            {
                headerStringBuilder.Append($"{outerClassName}::");
            }

            if (!string.IsNullOrWhiteSpace(className))
            {
                headerStringBuilder.Append($"{className}::");
            }

            if (!string.IsNullOrWhiteSpace(methodName))
            {
                headerStringBuilder.Append($"{methodName}::");
            }

            return headerStringBuilder.ToString();
        }

        /// <summary>
        /// need to "pre-initialize" the logging engine with some default values, so that if
        ///    we're looking at the console, we'll have the opportunity to display and persist
        ///    logs starting from the highest point in the code path, (Main entry)
        ///    even before the IOrcaApplicationConfigurationRepository.DeserializeRepository()
        ///    is called
        /// realistically, "pre-initializing" the logging engine captures a few lines of logs of the logging
        ///    engine intializing, and expands the applications ability to log catastrophic failures,
        ///    at application startup
        ///          ex: in the case of a configuration problem (before logging levels are read from repo)
        /// </summary>
        public static void PreInitializeApplicationLog()
        {
            // Hardcoded values for application startup (before app.config values are loaded)
            var defaultGlobalLoggerLevelRange =
                new Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel>
                    (OrcaApplicationLogLevel.Debug, OrcaApplicationLogLevel.Fatal);

            OrcaApplicationLoggerGlobal.Initialize(defaultGlobalLoggerLevelRange);
        }

        /// <summary>
        /// This is used for a Fatal error / SystemException application-wide
        /// </summary>
        /// <param name="e"></param>
        public static void DieHorribly(Exception e)
        {
            Console.WriteLine(@"   _____");
            Console.WriteLine(@" //     \");
            Console.WriteLine(@"|| R.I.P |");
            Console.WriteLine(@"||       |");
            Console.WriteLine(@"||       |");
            Console.WriteLine(@"||     ,,i!!i,");
            Console.WriteLine(@"''""'',,}{,,");
            Console.WriteLine();
            Console.WriteLine("Sorry. ORCA died!");
            Console.WriteLine();
            Console.WriteLine("Move on with your life.");
            Console.WriteLine();
            Console.WriteLine($"======> (Post-mortem) '{e.Message}'");
            Console.WriteLine();
            Console.WriteLine("Press [Enter] to exit...");
            Console.WriteLine();
            Console.WriteLine();

            Console.ReadLine();

            Environment.Exit(-1); // hardcoded custom error code is baaaaaad -- dujohnson check
        }
    }
}