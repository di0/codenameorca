﻿using System;

namespace codenameORCA2.ApplicationLog
{
    public class OrcaApplicationLoggerNLog : IOrcaApplicationLogger
    {
        private readonly NLog.Logger _nLogLogger;

        public OrcaApplicationLoggerNLog(Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> globalLoggerLevelRange)
        {
            Initialize(globalLoggerLevelRange);

            _nLogLogger = NLog.LogManager.GetCurrentClassLogger();
        }

        public static NLog.LogLevel MapToNLogLevel(OrcaApplicationLogLevel orcaApplicationLogLevel)
        {
            switch (orcaApplicationLogLevel)
            {
                case OrcaApplicationLogLevel.Debug:
                    return NLog.LogLevel.Debug;

                case OrcaApplicationLogLevel.Info:
                    return NLog.LogLevel.Info;

                case OrcaApplicationLogLevel.Warning:
                    return NLog.LogLevel.Warn;

                case OrcaApplicationLogLevel.Error:
                    return NLog.LogLevel.Error;

                case OrcaApplicationLogLevel.Fatal:
                    return NLog.LogLevel.Fatal;

                case OrcaApplicationLogLevel.Off:
                    return NLog.LogLevel.Off;

                default:
                    throw new NotImplementedException(OrcaApplicationLogUtilities.GetLoggingHeader());
            }
        }

        /// <summary>
        /// Allows reinitialization of the NLogLogger (since GlobalLogger is static and always
        ///    starts first, we need to be able to reconfigure NLog at an arbitrary time during app startup
        /// </summary>
        /// <param name="globalLoggerLevelRange"></param>
        public void Initialize(Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> globalLoggerLevelRange)
        {
            NLog.LogManager.Configuration =
                OrcaApplicationLoggerConfigurationNLog.Initialize(
                    new NLog.Config.LoggingConfiguration(), globalLoggerLevelRange);
        }

        public void Log(string logHeader, string logMessage, OrcaApplicationLogLevel orcaApplicationLogLevel)
        {
            var nLogEventInfo = new NLog.LogEventInfo();

            var nLogLevel = MapToNLogLevel(orcaApplicationLogLevel);

            nLogEventInfo.Properties["level"] = nLogLevel.ToString();
            nLogEventInfo.Properties["timestamp"] = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"); // ISO 8601 + UTC, but missing timezone / offset

            nLogEventInfo.Level = nLogLevel;

            nLogEventInfo.Message = logHeader + logMessage;

            _nLogLogger.Log(nLogEventInfo);
        }
    }
}