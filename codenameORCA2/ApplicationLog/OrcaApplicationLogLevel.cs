﻿namespace codenameORCA2.ApplicationLog
{
    public enum OrcaApplicationLogLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        Fatal,
        Off
    }
}