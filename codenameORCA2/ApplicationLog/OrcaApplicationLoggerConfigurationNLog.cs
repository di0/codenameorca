﻿using NLog;
using NLog.Common;
using NLog.Targets;
using NLog.Time;
using System;

namespace codenameORCA2.ApplicationLog
{
    /// <summary>
    /// This class was created so that we don't have to include NLog.config files with our release.
    /// NLog.config was the way we used to do it -- now we do it the programmatic way, but the
    /// effect is identical to how the .config files worked.
    /// </summary>
    public class OrcaApplicationLoggerConfigurationNLog : IOrcaApplicationLoggerConfiguration
    {
        /// <summary>
        /// The only public interface for this class (takes an instance of NLog.Config.LoggingConfiguration
        ///    and customizes it for the ORCA product.
        /// </summary>
        /// <param name="nLogConfig"></param>
        /// <param name="globalLoggerLevelRange"></param>
        public static NLog.Config.LoggingConfiguration Initialize(NLog.Config.LoggingConfiguration nLogConfig,
            Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> globalLoggerLevelRange)
        {
            initNLogGlobals();

            //
            // NLOG TARGET: File
            //
            var nLogFileTarget = createNLogFileTarget();
            nLogConfig.AddTarget("file", nLogFileTarget);

            //
            // NLOG TARGET: ColoredConsole
            //
            var nLogColoredConsoleTarget = new ColoredConsoleTarget { DetectConsoleAvailable = true };
            nLogConfig.AddTarget("console", nLogColoredConsoleTarget);

            //
            // NLOG RULES: Add logging rules for each NLog target
            //

            // convert from OrcaApplicationLogLevel to NLogLevel
            OrcaApplicationLogLevel orcaApplicationLogLevelMin = globalLoggerLevelRange.Item1;
            OrcaApplicationLogLevel orcaApplicationLogLevelMax = globalLoggerLevelRange.Item2;
            NLog.LogLevel nlogLevelMin = OrcaApplicationLoggerNLog.MapToNLogLevel(orcaApplicationLogLevelMin);
            NLog.LogLevel nLogLevelMax = OrcaApplicationLoggerNLog.MapToNLogLevel(orcaApplicationLogLevelMax);

            // add the rules
            nLogConfig.AddRule(nlogLevelMin, nLogLevelMax, nLogFileTarget);
            nLogConfig.AddRule(nlogLevelMin, nLogLevelMax, nLogColoredConsoleTarget);

            return nLogConfig;
        }

        /// <summary>
        /// Make a FileTarget for NLog
        /// </summary>
        /// <returns>Our NLog FileTarget</returns>
        private static FileTarget createNLogFileTarget()
        {
            const int ARCHIVE_SIZE = 10000000;
            const int MAX_ARCHIVES = 10;
            const int TIMEOUT      = 30;

            var thisAssemblyModule = System.Reflection.Assembly.GetEntryAssembly()?.EntryPoint?.Module;

            // handles calling code that is not an assembly (like unit tests)
            var logFileName = thisAssemblyModule?.Name ?? "NLogFileTargetFallback";

            return new FileTarget
            {
                FileName             = logFileName + ".log",
                ArchiveFileName      = logFileName + ".{#}.log",
                ArchiveDateFormat    = "yyyyMMdd",
                ArchiveNumbering     = ArchiveNumberingMode.DateAndSequence,
                ArchiveAboveSize     = ARCHIVE_SIZE,
                MaxArchiveFiles      = MAX_ARCHIVES,
                ConcurrentWrites     = true,
                KeepFileOpen         = true,
                OpenFileCacheTimeout = TIMEOUT,
                CleanupFileName      = false
            };
        }

        /// <summary>
        /// Set some necessary global NLog settings
        /// </summary>
        private static void initNLogGlobals()
        {
            TimeSource.Current = new FastUtcTimeSource(); // Set TimeSource

            // Send internal NLog messages (including debug) to console
            //    (should be false for production)
            InternalLogger.LogToConsole = true;

            // Disable NLog exception throwing
            //    (should be false for production)
            LogManager.ThrowExceptions = true;
        }
    }
}