﻿using System;
using System.Threading.Tasks;
using codenameORCA2.ApplicationLog;
using codenameORCA2.Instance;

namespace codenameORCA2
{
    /*

            ~                           ~              ~
               ~~~~     ~          ~~ ~        ~      ~    ~~
          ~~             _,-''-.     ~~        .-._       ~  ~~~
                    ,---':::::::\`.            \_::`.,...__    ~
             ~     |::`.:::::::::::`.       ~    )::::::.--'
                   |:_:::`.::::::::::`-.__~____,'::::(
         ~~~~       \```-:::`-.o:::::::::\:::::::::~::\       ~~~
                     )` `` `.::::::::::::|:~~:::::::::|      ~   ~~
         ~~        ,',' ` `` \::::::::,-/:_:::::::~~:/
                 ,','/` ,' ` `\::::::|,'   `::~~::::/  ~~        ~
        ~       ( (  \_ __,.-' \:-:,-'.__.-':::::::'  ~    ~
            ~    \`---''   __..--' `:::~::::::_:-'
                  `------''      ~~  \::~~:::'
               ~~   `--..__  ~   ~   |::_:-'                    ~~~
           ~ ~~     /:,'   `''---.,--':::\          ~~       ~~
          ~         ``           (:::::::|  ~~~            ~~    ~
        ~~      ~~             ~  \:~~~:::             ~       ~~~
                     ~     ~~~     \:::~::          ~~~     ~
            ~~ jrei      ~~    ~~~  ::::::                     ~~
                  ~~~                \::::   ~~
                               ~   ~~ `--'

     */

    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                // first thing's first -- get some basic logging going
                OrcaApplicationLogUtilities.PreInitializeApplicationLog();

                OrcaApplicationLoggerGlobal.Log
                (OrcaApplicationLogUtilities.GetLoggingHeader(),
                    "ORCA application is starting up... Press [ENTER] to exit.",
                    OrcaApplicationLogLevel.Info);

                // create us a good ole bootstrapper to keep it loose
                //    - bootstrapper loads this application's configuration from the
                //      configured OrcaApplicationConfigurationRepository (currently using app.config)
                IOrcaBootstrapper orcaBootstrapper = new OrcaBootstrapper();

                // start all of the OrcaInstances found
                foreach (IOrcaInstance orcaInstance in orcaBootstrapper.OrcaInstances)
                {
                    // start every OrcaInstance Async
                    Task.Run(async () =>
                    {
                        try
                        {
                            await orcaInstance.StartAsync();

                        }
                        catch (Exception e)
                        {
                            OrcaApplicationLogUtilities.DieHorribly(e);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                OrcaApplicationLogUtilities.DieHorribly(e);
            }

            Console.ReadLine();
        }
    }
}