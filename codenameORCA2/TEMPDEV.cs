﻿namespace codenameORCA2
{
    public enum AssetFramework_DenialOfServiceEnum
    {
        WindowsServiceRestart,
        Kb23456,
        Kb85899,
        Kb92398
    }

    public enum DisruptiveLoadType
    {
        HeavyNetwork,
        HeavyCpu,
        HeavyRam,
        HeavyDisk
    }

    public enum KnownWindowsServiceNames
    {
        // MS SQL Server
        //    note: all require a suffix in the form of <servicename>$<instancename>
        MSSQLFDLauncher, // SQL Full-text Filter Daemon Launcher

        MSSQL, // SQL Server

        // OSIsoft AF Server
        AFService,

        PIAnalysisManager,
        piarchss, // PI Archive Subsystem

        AAAANDKEEPGOING
    }

    public enum MsSql_DenialOfServiceEnum // specific to each target type
    {
        WindowsServiceRestart,
        SomeOtherKnownDosMethod
    }
}