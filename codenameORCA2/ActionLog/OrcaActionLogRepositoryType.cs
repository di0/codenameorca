﻿namespace codenameORCA2.ActionLog
{
    public enum OrcaActionLogRepositoryType
    {
        Csv,
        MsSql
    }
}