﻿using System;
using System.Collections.Generic;

namespace codenameORCA2.ActionLog
{
    public interface IOrcaActionLogRepository
    {
        List<OrcaCatastropheAction> GetOrcaTransactions(DateTime fromDateTime, DateTime toDateTime);

        List<OrcaCatastropheAction> GetOrcaTransactions(int runId);

        bool PutOrcaTransaction();
    }
}