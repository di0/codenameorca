﻿using System.Threading;
using codenameORCA2.State;
using codenameORCA2.TargetHost;
using codenameORCA2.TargetHost.Resource;
using codenameORCA2.TargetHost.Resource.Catastrophe;
using System.Threading.Tasks;

namespace codenameORCA2.ActionLog
{
    /// <summary>
    /// OrcaCatastropheAction are primarily what OrcaActionLogs are made of...
    ///    (as of right now 2018.12.17, they are the only type of IOrcaAction,
    ///     but we may add more types of IOrcaActions in the future)
    /// </summary>
    public class OrcaCatastropheAction : IOrcaAction
    {
        /*
			implement feature:  OrcaCatastropheAction + OrcaActionLog

				OrcaActionLog table design

					OrcaJobId
					OrcaJobTimestamp
					OrcaTargetConfiguration??? is this necessary
					OrcaWorkerNodeId
					OrcaTargetHost
					OrcaTargetHostResource
					OrcaTargetHostResourceCatastrophe
					OrcaState
					JobStartOffsetTicks
					ActualTicks
		 */

        public OrcaCatastropheAction()
        {
            State = OrcaStateType.Stopped; // should be OrcaFsm.InitialState
        }

        public long ActionTimestampTicks { get; set; }

        public long JobStartTimestamp { get; set; }

        public long JobStartTimestampOffsetTicks { get; set; }

        public IOrcaTargetHost OrcaTargetHost { get; set; }

        public IOrcaTargetHostResource OrcaTargetHostResource { get; set; }

        public IOrcaTargetHostResourceCatastrophe OrcaTargetHostResourceCatastrophe { get; set; }

        public long OrcaWorkerNodeId { get; set; }

        public CancellationToken CancelToken { get; set; }

        public OrcaStateType State { get; set; }

        public void Fail(string failMessage)
        {
            throw new System.NotImplementedException();
        }

        public void Start()
        {
            throw new System.NotImplementedException();
        }

        public async Task StartAsync()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }

        public void Succeed()
        {
            throw new System.NotImplementedException();
        }

        public void SucceedWithWarning(string warnMessage)
        {
            throw new System.NotImplementedException();
        }
    }
}