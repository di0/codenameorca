﻿using codenameORCA2.State;

namespace codenameORCA2.ActionLog
{
    public interface IOrcaAction : IOrcaState
    {
        long OrcaWorkerNodeId { get; set; }
    }
}