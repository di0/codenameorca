﻿using codenameORCA2.ApplicationConfiguration;
using codenameORCA2.ApplicationConfiguration.RepositoryAppConfig;
using codenameORCA2.ApplicationLog;
using System;
using System.Collections.Generic;
using System.Threading;

namespace codenameORCA2.Instance
{
    public class OrcaBootstrapper : IOrcaBootstrapper
    {
        public OrcaBootstrapper(
            IOrcaApplicationConfiguration orcaApplicationConfiguration = null,
            CancellationToken? cancellationToken = null)
        {
            // initialize OrcaInstances so we can add 'em at will
            OrcaInstances = new List<IOrcaInstance>();

            try
            {
                // if an OrcaApplicationConfiguration was passed to the bootstrapper, then use it,
                //    else, create one deserialized from an OrcaApplicationConfigurationRepository
                if (orcaApplicationConfiguration != null) // we got a live one
                {
                    foreach (IOrcaInstanceConfiguration orcaInstanceConfiguration in
                        orcaApplicationConfiguration.OrcaInstanceConfigurations)
                    {
                        OrcaInstances.Add(new OrcaInstance(orcaInstanceConfiguration, cancellationToken));
                    }
                }
                else // orcaApplicationConfiguration == null
                {
                    // create a new instance of a OrcaApplicationConfigurationRepository,
                    //    so we can try and load a config from it
                    IOrcaApplicationConfigurationRepository appConfigRepository =
                        new OrcaApplicationConfigurationRepositoryAppConfig();

                    // create a new application configuration for this application instance
                    orcaApplicationConfiguration = new OrcaApplicationConfiguration();

                    // now load the app.config values into the OrcaApplicationConfiguration
                    orcaApplicationConfiguration =
                        appConfigRepository.DeserializeRepository(
                            orcaApplicationConfiguration);

                    // temporary architectural placeholder
                    temporaryTestingCodeToCreateInstance(cancellationToken);



                    // make sure we can continue
                    if ((orcaApplicationConfiguration?.OrcaInstanceConfigurations == null
                        || orcaApplicationConfiguration.OrcaInstanceConfigurations.Count == 0)
                            && OrcaInstances.Count == 0)
                    {
                        // uh oh. repo came back with no OrcaInstanceConfigurations.
                        //    we kinda need those.
                        OrcaApplicationLoggerGlobal.Log(
                            OrcaApplicationLogUtilities.GetLoggingHeader(),
                            $"The '{nameof(OrcaApplicationConfigurationRepositoryAppConfig)}' " +
                            $"did not return a valid '{nameof(OrcaInstanceConfiguration)}'.",
                            OrcaApplicationLogLevel.Fatal);

                        throw new SystemException("Dunno what to do without an 'OrcaInstanceConfiguration' to work with... :/ womp whomp.");
                    }
                }
            }
            catch (Exception e)
            {
                // Poor thing never even had a chance.
                OrcaApplicationLogUtilities.DieHorribly(e);
            }
        }

        public List<IOrcaInstance> OrcaInstances { get; set; }



        // FOR HARDCODE TESTING BELOW UNTIL APP.CONFIG WORKS
        private void temporaryTestingCodeToCreateInstance(CancellationToken? cancellationToken)
        {
            // create a single orca instance configuration set
            IOrcaInstanceConfiguration orcaInstanceConfiguration = new OrcaInstanceConfiguration();

            // create new instance of ORCA, with the configuration set we just initialized
            //    after vertical layers have been initialized (logging, exception handling) -- is this line needed? dujohnson
            OrcaInstances.Add(new OrcaInstance(orcaInstanceConfiguration, cancellationToken));
        }
    }
}