﻿using System;
using System.Collections.Generic;

namespace codenameORCA2.Instance
{
    public interface IOrcaBootstrapper
    {
        List<IOrcaInstance> OrcaInstances { get; set; }
    }
}