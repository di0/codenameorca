﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.Instance;
using codenameORCA2.TargetHost;
using codenameORCA2.TargetHost.Resource;
using codenameORCA2.TargetHost.Resource.Catastrophe;
using System;
using System.Threading.Tasks;

namespace codenameORCA2
{
    internal class OrcaInstanceActor
    {
        private static readonly Random _randomInstance = new Random();

        public static async Task<bool> TryAllTargetHostsAllResourcesAllCatastrophiesOrderedAsync(IOrcaInstance orcaInstance)
        {
            while (!orcaInstance.CancelToken.IsCancellationRequested)
            {
                foreach (var orcaTargetHost in orcaInstance.OrcaInstanceConfiguration.OrcaTargetHosts)
                {
                    foreach (var orcaTargetHostResource in orcaTargetHost.OrcaTargetHostConfiguration.OrcaTargetHostResources)
                    {
                        foreach (var orcaTargetHostResourceCatastrophe in orcaTargetHostResource.OrcaTargetHostResourceCatastrophies)
                        {
                            await orcaTargetHost.TryCatastrophe(orcaTargetHostResource, orcaTargetHostResourceCatastrophe);
                        }
                    }
                }

                await Task.Delay(2000);

                OrcaApplicationLoggerGlobal.Log(OrcaApplicationLogUtilities.GetLoggingHeader(), "Completed iteration...", OrcaApplicationLogLevel.Debug);
            }

            return false;
        }

        public static async Task<bool> TryAllTargetHostsAllResourcesAllCatastrophiesRandomAsync(IOrcaInstance orcaInstance)
        {
            while (!orcaInstance.CancelToken.IsCancellationRequested)
            {
                //
                // OrcaTargetHosts
                //

                // count the OrcaTargetHosts on the OrcaInstances
                int orcaTargetHostCount = orcaInstance.OrcaInstanceConfiguration.OrcaTargetHosts.Count;

                // get a random OrcaTargetHosts index
                int orcaTargetHostIndex = _randomInstance.Next(orcaTargetHostCount);

                // get the OrcaTargetHost at that index
                IOrcaTargetHost orcaTargetHost = orcaInstance.OrcaInstanceConfiguration.OrcaTargetHosts[orcaTargetHostIndex];

                //
                // OrcaTargetResources
                //

                // count the OrcaTargetHostResources on this OrcaTargetHost
                int orcaTargetHostResourcesCount =
                    orcaTargetHost.OrcaTargetHostConfiguration.OrcaTargetHostResources.Count;

                // get a random OrcaTargetHostResources index
                int orcaTargetHostResourceIndex = _randomInstance.Next(orcaTargetHostResourcesCount);

                IOrcaTargetHostResource orcaTargetHostResource =
                    orcaTargetHost.OrcaTargetHostConfiguration.OrcaTargetHostResources[orcaTargetHostResourceIndex];

                //
                // OrcaTargetHostResourceCatastrophies
                //

                // count the OrcaTargetHostResourceCatastrophies for this OrcaTargetHostResource
                int orcaTargetHostResourceCatastrophyCount = orcaTargetHostResource.OrcaTargetHostResourceCatastrophies.Count;

                // get a random OrcaTargetHostResourceCatastrophy index
                int orcaTargetHostResourceCatastrophyIndex = _randomInstance.Next(orcaTargetHostResourceCatastrophyCount);

                IOrcaTargetHostResourceCatastrophe orcaTargetHostResourceCatastrophy =
                    orcaTargetHostResource.OrcaTargetHostResourceCatastrophies[orcaTargetHostResourceCatastrophyIndex];

                //
                // LAY WASTE
                //

                await orcaTargetHost.TryCatastrophe(orcaTargetHostResource, orcaTargetHostResourceCatastrophy);

                await Task.Delay(5000);

                OrcaApplicationLoggerGlobal.Log(OrcaApplicationLogUtilities.GetLoggingHeader(), "Completed iteration...", OrcaApplicationLogLevel.Debug);
            }

            return false;
        }
    }
}