﻿using codenameORCA2.State;
using System.Threading;
using System.Threading.Tasks;

namespace codenameORCA2.Instance
{
    public interface IOrcaInstance : IOrcaState
    {
        IOrcaInstanceConfiguration OrcaInstanceConfiguration { get; set; }

        Task RunOrcaJobsAsync();
    }
}