﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.State;
using System;
using System.Threading;
using System.Threading.Tasks;
using codenameORCA2.Job;

namespace codenameORCA2.Instance
{
    public class OrcaInstance : IOrcaInstance
    {
        /// <summary>
        /// You can't create a new OrcaInstance without a valid OrcaInstanceConfiguration
        /// </summary>
        /// <param name="orcaInstanceConfiguration"></param>
        /// <param name="cancellationToken"></param>
        public OrcaInstance(
            IOrcaInstanceConfiguration orcaInstanceConfiguration,
            CancellationToken? cancellationToken = null)
        {
            OrcaInstanceConfiguration = orcaInstanceConfiguration;

            CancelToken = cancellationToken ?? new CancellationToken();
        }

        public CancellationToken CancelToken { get; set; }

        public IOrcaInstanceConfiguration OrcaInstanceConfiguration { get; set; }

        /// <summary>
        /// We're assuming we have OrcaJobs attached to the orcaInstance here...
        /// </summary>
        public async Task RunOrcaJobsAsync()
        {
            if (OrcaInstanceConfiguration.OrcaJobs == null
                || OrcaInstanceConfiguration.OrcaJobs.Count.Equals(0))
            {
                // there are no jobs, so just log that and throw

                OrcaApplicationLoggerGlobal.Log(
                    OrcaApplicationLogUtilities.GetLoggingHeader(),
                    $"No '{nameof(OrcaJob)}' found on the specified '{nameof(OrcaInstanceConfiguration)}'.",
                    OrcaApplicationLogLevel.Fatal);

                throw new SystemException("Dunno what to do without 'OrcaJobs' to work with... :/ womp whomp.");
            }

            // prototype test ORCA job 1 async
            Task.Run(async () =>
            {
                await OrcaInstanceActor.TryAllTargetHostsAllResourcesAllCatastrophiesRandomAsync(this);
            });

            // prototype test ORCA job 2 async
            Task.Run(async () =>
            {
                await OrcaInstanceActor.TryAllTargetHostsAllResourcesAllCatastrophiesOrderedAsync(this);
            });
        }

        #region IOrcaState Implementation

        public OrcaStateType State { get; set; }

        public void Fail(string failMessage)
        {
            OrcaApplicationLoggerGlobal.Log(
                OrcaApplicationLogUtilities.GetLoggingHeader(),
                failMessage,
                OrcaApplicationLogLevel.Error);

            State = OrcaStateType.Failed;
        }

        public void Start()
        {
            State = OrcaStateType.Started;
        }

        public async Task StartAsync()
        {
            OrcaApplicationLoggerGlobal.Log
            (OrcaApplicationLogUtilities.GetLoggingHeader(),
                "OrcaInstance is starting up...",
                OrcaApplicationLogLevel.Info);

            State = OrcaStateType.Started;

            //CreateJobs(); // not sure this makes sense logically -- leaving here for now

            // I AM BECOME DEATH, DESTROYER OF WORLDS
            await RunOrcaJobsAsync();

            // all tasks run async, so leave console open
            Console.ReadLine(); // thiiisss doesn't seem like it should live here --- dujohnson check
        }

        /// <summary>
        /// Sends Cancellation request to any currently running Async OrcaJobs for this OrcaInstance
        /// </summary>
        /// <param name="orcaStateType"></param>
        public void Stop()
        {
            OrcaApplicationLoggerGlobal.Log(
                OrcaApplicationLogUtilities.GetLoggingHeader(),
                "Stopping ORCA Instance (Requesting Cancellation For All Async Jobs)...",
                OrcaApplicationLogLevel.Warning);

            // create a new cts and set it to the OrcaInstances, then cancel it
            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                CancelToken = cancellationTokenSource.Token;

                cancellationTokenSource.Cancel();
            }

            while (State == OrcaStateType.Started)
            {
                // TODO: implement retry logic utilities class --- dujohnson (currently infinite loops)

                Thread.Sleep(1000);

                OrcaApplicationLoggerGlobal.Log(
                    OrcaApplicationLogUtilities.GetLoggingHeader(),
                    $"Waiting for ORCA Instance...",
                    OrcaApplicationLogLevel.Warning);
            }

            OrcaApplicationLoggerGlobal.Log(
                OrcaApplicationLogUtilities.GetLoggingHeader(),
                $"ORCA Instance is {OrcaStateType.Stopped}.",
                OrcaApplicationLogLevel.Warning);

            State = OrcaStateType.Stopped;
        }

        public void Succeed()
        {
            OrcaApplicationLoggerGlobal.Log(
                OrcaApplicationLogUtilities.GetLoggingHeader(),
                "Success",
                OrcaApplicationLogLevel.Warning);

            State = OrcaStateType.Succeeded;
        }

        public void SucceedWithWarning(string warnMessage)
        {
            OrcaApplicationLoggerGlobal.Log(
                OrcaApplicationLogUtilities.GetLoggingHeader(),
                warnMessage,
                OrcaApplicationLogLevel.Warning);

            State = OrcaStateType.SucceededWithWarning;
        }

        #endregion IOrcaState Implementation
    }
}