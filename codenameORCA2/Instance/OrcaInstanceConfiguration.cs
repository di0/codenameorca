﻿using codenameORCA2.ApplicationLog;
using codenameORCA2.Job;
using codenameORCA2.TargetHost;
using System;
using System.Collections.Generic;

namespace codenameORCA2.Instance
{
    public class OrcaInstanceConfiguration : IOrcaInstanceConfiguration
    {
        private Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> _globalLoggerLevelRange;

        public OrcaInstanceConfiguration()
        {
            OrcaTargetHosts = new List<IOrcaTargetHost>();
        }

        public Tuple<OrcaApplicationLogLevel, OrcaApplicationLogLevel> ApplicationLoggerLevelRange
        {
            get => _globalLoggerLevelRange;

            set
            {
                _globalLoggerLevelRange = value;

                OrcaApplicationLoggerGlobal.Initialize(_globalLoggerLevelRange);
            }
        }

        public List<IOrcaJob> OrcaJobs { get; set; }
        public List<IOrcaTargetHost> OrcaTargetHosts { get; set; }
    }
}