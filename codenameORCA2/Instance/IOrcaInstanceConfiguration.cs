﻿using codenameORCA2.Job;
using codenameORCA2.TargetHost;
using System.Collections.Generic;

namespace codenameORCA2.Instance
{
    public interface IOrcaInstanceConfiguration
    {
        List<IOrcaJob> OrcaJobs { get; set; }
        List<IOrcaTargetHost> OrcaTargetHosts { get; set; }
    }
}