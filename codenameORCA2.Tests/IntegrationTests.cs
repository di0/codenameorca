﻿using codenameORCA2.ApplicationConfiguration;
using codenameORCA2.Instance;
using codenameORCA2.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace codenameORCA2.Tests
{
    // Integration Tests
    [TestClass]
    public class IntegrationTests
    {
        /// <summary>
        /// Minimum Run Time: 5 seconds (sleep)
        /// </summary>
        [TestMethod]
        public void OrcaBootstrapper_StartOrcaInstanceAsync_WithDummyTargetHosts_OrcaInstanceStateStartsAndStopsWhenExpected()
        {
            // ARRANGE: create a mock OrcaApplicationConfiguration
            IOrcaApplicationConfiguration orcaApplicationConfiguration = new OrcaApplicationConfiguration();

            // ARRANGE: create a mock OrcaInstanceConfiguration
            IOrcaInstanceConfiguration orcaInstanceConfigurationDummy =
                new OrcaInstanceConfiguration();

            // ARRANGE: add some mock TargetHosts to the OrcaInstanceConfiguration
            orcaInstanceConfigurationDummy.AddTargetHostDummy();
            orcaInstanceConfigurationDummy.AddTargetHostDummy();
            orcaInstanceConfigurationDummy.AddTargetHostDummy();
            orcaInstanceConfigurationDummy.AddTargetHostDummy();
            orcaInstanceConfigurationDummy.AddTargetHostDummy();

            // ARRANGE: add the mock OrcaInstanceConfiguration to the OrcaApplicationConfiguration
            orcaApplicationConfiguration.OrcaInstanceConfigurations.Add(orcaInstanceConfigurationDummy);

            // ARRANGE: simulate app startup with mock configuration
            IOrcaBootstrapper orcaBootstrapper =
                new OrcaBootstrapper(orcaApplicationConfiguration);

            //
            //
            // The following tests cover the State machine
            //    - each loop should be its own test in a test class
            //
            //

            // Test StartAsync()
            foreach (IOrcaInstance orcaInstance in orcaBootstrapper.OrcaInstances)
            {
                // ASSERT: make sure instances show .Stopped, then .Started after StartAsync()
                Assert.IsTrue(orcaInstance.State == OrcaStateType.Stopped);

                Task.Run(async () =>
                {
                    // ACT
                    await orcaInstance.StartAsync();

                    // ASSERT: make sure instances show .Stopped, then .Started after StartAsync()
                    Assert.IsTrue(orcaInstance.State == OrcaStateType.Started);
                });
            }

            // Test Stop()
            foreach (IOrcaInstance orcaInstance in orcaBootstrapper.OrcaInstances)
            {
                // ACT
                orcaInstance.Stop();

                // ASSERT: make sure instances show .Stopped after Stop()
                Assert.IsTrue(orcaInstance.State == OrcaStateType.Stopped);
            }

            // Test Succeed()
            foreach (IOrcaInstance orcaInstance in orcaBootstrapper.OrcaInstances)
            {
                // ACT
                orcaInstance.Succeed();

                // ASSERT: make sure instances show .Succeeded after Succeed()
                Assert.IsTrue(orcaInstance.State == OrcaStateType.Succeeded);
            }

            // Test SucceedWithWarning()
            foreach (IOrcaInstance orcaInstance in orcaBootstrapper.OrcaInstances)
            {
                // ACT
                orcaInstance.SucceedWithWarning("mock warning on succeed");

                // ASSERT: make sure instances show .SuccededWithWarning after SucceedWithWarning()
                Assert.IsTrue(orcaInstance.State == OrcaStateType.SucceededWithWarning);
            }
        }
    }
}