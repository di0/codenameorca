﻿using codenameORCA2.Instance;
using codenameORCA2.TargetHost;
using codenameORCA2.TargetHost.Resource;
using codenameORCA2.TargetHost.Resource.Catastrophe;

namespace codenameORCA2.Tests
{
    public static class TestUtilities
    {
        public static IOrcaInstanceConfiguration AddTargetHostDummy(
            this IOrcaInstanceConfiguration orcaInstanceConfiguration)
        {
            //
            // OrcaTargetHostResource
            //

            // create a new OrcaTargetHostResourceConfiguration
            IOrcaTargetHostResourceConfiguration orcaTargetHostResourceConfiguration =
                new OrcaTargetHostResourceConfigurationDummy();

            // create a new OrcaTargetHostResource
            IOrcaTargetHostResource orcaTargetHostResource =
                OrcaTargetHostResourceFactory.Create(OrcaTargetHostResourceType.Dummy,
                    orcaTargetHostResourceConfiguration);

            //
            // OrcaTargetHostResourceCatastrophe
            //

            // create a new OrcaTargetHostResourceCatastrophe on the OrcaTargetHostResource
            IOrcaTargetHostResourceCatastrophe dummyOrcaTargetHostResourceCatastrophe =
                OrcaTargetHostResourceCatastropheFactory.Create(
                    OrcaTargetHostResourceCatastropheType.Dummy);

            // add the OrcaTargetHostResourceCatastrophe to the the OrcaTargetHostResource
            orcaTargetHostResource.OrcaTargetHostResourceCatastrophies.Add(dummyOrcaTargetHostResourceCatastrophe);

            //
            // OrcaTargetHostConfiguration
            //

            // create a new OrcaTargetHostConfiguration (required to create the OrcaTargetHost)
            IOrcaTargetHostConfiguration orcaTargetHostConfiguration = new OrcaTargetHostConfiguration();

            // add the OrcaTargetHostResource to the OrcaTargetHostConfiguration
            orcaTargetHostConfiguration.OrcaTargetHostResources.Add(orcaTargetHostResource);

            //
            // OrcaTargetHost
            //

            // create the OrcaTargetHost, passing the OrcaTargetHostConfiguration
            IOrcaTargetHost dummyTargetHost =
                OrcaTargetHosts.OrcaTargetHostFactory.Create(OrcaTargetHostType.Dummy, orcaTargetHostConfiguration);

            // add the OrcaTargetHost to the OrcaInstanceConfiguration
            orcaInstanceConfiguration.OrcaTargetHosts.Add(dummyTargetHost);

            return orcaInstanceConfiguration;
        }
    }
}